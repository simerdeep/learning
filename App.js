
import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';

import AppIntroSlider from './src/appintroslider';
import { Provider } from 'react-redux';
import configureStore from './src/configureStore';
import { persistStore, autoRehydrate } from 'redux-persist';
//import { LoadingIndicator } from './src/components/loadingindicator';

export default class App extends Component {

  constructor() {

    super();
    this.state = {
      isLoading: false,
      store: configureStore(() => this.setState({ isLoading: false })),
      rehydrated: false,
    };

  }

  componentWillMount(){

    persistStore(this.state.store, { storage: AsyncStorage }, () => {

      this.setState({ rehydrated: true })
    });

  }

  render() {

    if(!this.state.rehydrated) {
      return null;
    }
    else {

      return (
        <Provider store = { this.state.store }>
          <AppIntroSlider />
        </Provider>

      );

    }


  }

}
