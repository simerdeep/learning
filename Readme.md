# Learning


## Get Started


#### 1. Installation

On the command prompt run the following commands

```sh
$ git clone https://simerdeep@bitbucket.org/simerdeep/learning.git

$ cd Learning

$ npm install -g react-native-cli

$ npm install

$ react-native link
```

#### 2. Simulate for iOS

**Method One**

*	Open the project in Xcode from **ios/Learning.xcodeproj**.

*	Hit the play button.


**Method Two**

*	Run the following command in your terminal.

```sh
$ react-native run-ios
```

#### 3. Simulate for Android

*	Make sure you have an **Android emulator** installed and running.

*	Run the following command in your terminal.

```sh
$ react-native run-android
```
