import { createStackNavigator, createAppContainer } from 'react-navigation';

import BottomTabNavigation from './bottomtabnavigation';
import LoginScreen from './screens/loginscreen';

const MainNavigator = createStackNavigator(

  {
    bottomNavigation: BottomTabNavigation,
    loginScreen: LoginScreen
  },
  {
      initialRouteName: "loginScreen",
      headerMode: 'none',

  }

);



const App = createAppContainer(MainNavigator);

export default App;
