import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider';
import { SCROLL_ICON_1, SCROLL_ICON_2, SCROLL_ICON_3 } from './utils/images';
import { getDeviceDimensions } from './utils';
import AppNavigation from './AppNavigation';
import { connect } from 'react-redux';
import { updateIntroSliderFlag } from './services/appactions';

const slides = [
  {
    key: 'Slider 1',
    title: 'Slide 1',
    text: 'This is Slide 1',
    image: SCROLL_ICON_1,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'Slider 2',
    title: 'Slide 2',
    text: 'This is Slide 2',
    image: SCROLL_ICON_2,
    backgroundColor: '#febe29',
  },
  {
    key: 'Slider 3',
    title: 'Slide 3',
    text: 'This is Slide 3',
    image: SCROLL_ICON_3,
    backgroundColor: '#22bcb5',
  },
  {
    key: 'Slider 4',
    title: 'Slide 4',
    text: 'This is Slide 4',
    image: SCROLL_ICON_3,
    backgroundColor: '#22bcb5',
  }
];

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    height: getDeviceDimensions().deviceHeight,
    width: getDeviceDimensions().deviceWidth,
    backgroundColor: "red"
  },
  image: {
    width: 320,
    height: 320,
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 22,
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});

class AppSlider extends Component {

  render() {

    console.log("Props are", this.props);

    if(this.props.isIntroToShow) {

      return (
          <AppIntroSlider
            renderItem = { this._renderItem }
            slides = { slides }
            onDone = { this._onDone }
            showPrevButton
          />
      );

    }
    else {
      return (

        <AppNavigation />
      );

    }



  }

  _renderItem = (item) => {
    return (
      <View style = {[ styles.mainContent, { backgroundColor : item.backgroundColor }] }>
        <Text style = { styles.title }>{ item.title }</Text>
        <Image style = { styles.image } source = { item.image } />
        <Text style = { styles.text }>{ item.text }</Text>
      </View>
    );
  }

  _onDone = () => {

    /*this.setState({
      isIntroToShow : false
    });*/

    this.props.updateIntroSliderFlag();

  }

}

function bindActions(dispatch) {

  return (
    {

      updateIntroSliderFlag: (params) => dispatch(updateIntroSliderFlag(params)),
    }
  );
}

function mapStateToProps(state) {


  return(
    {

    isIntroToShow: state.app.isIntroToShow,
    }
  );
}

export default connect(mapStateToProps, bindActions)(AppSlider);
