export const APP_RED_COLOR = "#d83542";
export const APP_BLACK_COLOR = "#000";
export const APP_WHITE_COLOR = "white";
export const APP_GREY_COLOR = '#595959';

export const APP_RED_GRADIENT1_COLOR = "#ff2119";
export const APP_RED_GRADIENT2_COLOR = "#e56a66";
export const APP_RED_GRADIENT3_COLOR = "#e5514c";

export const APP_RED_GRADIENT4_COLOR = "#d83542";
export const APP_RED_GRADIENT5_COLOR = "#d64558";
export const APP_RED_GRADIENT6_COLOR = "#d33b4b";
export const APP_RED_GRADIENT7_COLOR = "#df7282";

export const APP_PINK_GRADIENT1_COLOR = "#ee9ca7";
export const APP_PINK_GRADIENT2_COLOR = "#ffdde1";

export const APP_BLUE_GRADIENT1_COLOR = "#2193b0";
export const APP_BLUE_GRADIENT2_COLOR = "#6dd5ed";

export const APP_MEGATRON_GRADIENT1_COLOR = "#C6FFDD";
export const APP_MEGATRON_GRADIENT2_COLOR = "#FBD786";
export const APP_MEGATRON_GRADIENT3_COLOR = "#f7797d";

export const APP_JSHINE_GRADIENT1_COLOR = "#12c2e9";
export const APP_JSHINE_GRADIENT2_COLOR = "#c471ed";
export const APP_JSHINE_GRADIENT3_COLOR = "#f7797d";

export const APP_MEMARIANI_GRADIENT1_COLOR = "#aa4b6b";
export const APP_MEMARIANI_GRADIENT2_COLOR = "#6b6b83";
export const APP_MEMARIANI_GRADIENT3_COLOR = "#f7797d";

export const APP_HARVEY_GRADIENT1_COLOR = "#1f4037";
export const APP_HARVEY_GRADIENT2_COLOR = "#99f2c8";

export const APP_KYAMEH_GRADIENT1_COLOR = "#8360c3";
export const APP_KYAMEH_GRADIENT2_COLOR = "#2ebf91";

export const APP_SUMMERDOG_GRADIENT1_COLOR = "#a8ff78";
export const APP_SUMMERDOG_GRADIENT2_COLOR = "#78ffd6";
