import { createDrawerNavigator, createBottomTabNavigator } from 'react-navigation';
import React, { Component } from 'react';

import Home from './screens/bottomtabscreens/Home';
import Alerts from './screens/bottomtabscreens/Alerts';
import Subscribe from './screens/bottomtabscreens/Subscribe';
import Dashboard from './screens/bottomtabscreens/Dashboard';

import { getDeviceDimensions } from './utils';

export default bottomNavigation = createBottomTabNavigator({
  Home: { screen: Home },
  Alerts: { screen: Alerts },
  Subscribe: { screen: Subscribe },
  Dashboard: { screen: Dashboard },

},
{
    initialRouteName: "Home",

    tabBarOptions: {
    activeTintColor: '#e91e63',
    labelStyle: {
      fontSize: 14,
    },
    style : {
      padding: 5,
      height: (getDeviceDimensions().deviceHeight)*0.1
    },

  }
}

);
