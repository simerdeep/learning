import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';
import { BACK_ARROW_ICON } from '../../utils/images';

class Header extends Component {

  render() {

    console.log("propsreceived", this.props );

    console.log("headerpropsreceived",this.props);

    let { heading } = this.props;

    return (

      <View style = { styles.headerMainStyle }>

        { this.renderBackButton() }

        <View style = { styles.headingViewStyle }>

          <Text style = { styles.headingTextStyle }>
            { heading }
          </Text>

        </View>

      </View>

    );

  }

  renderBackButton() {

    if(this.props.isBackButtonRequired) {

      return(

        <View style = { styles.imageViewStyle }>

          <TouchableOpacity onPress = { () => this.props.navigation.goBack() }>

            <Image
              source = { BACK_ARROW_ICON }
            />

          </TouchableOpacity>

        </View>

      );

    }

  }

}

export default withNavigation(Header);
