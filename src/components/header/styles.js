//import * as appstyleguide from '../appstyleguide';
import { getDeviceDimensions } from '../../utils';

export default {

  headerMainStyle: {

    height: (getDeviceDimensions().deviceHeight)*0.1,
    width: getDeviceDimensions().deviceWidth,
    flexDirection: "row",

  },

  headingTextStyle: {

    fontSize: 18
  },

  imageViewStyle : {

    flex: 0.3,
    alignSelf: "center"

  },
  headingViewStyle: {

    flex: 0.7,
    alignSelf: "center",
    marginLeft: 10
  },




};
