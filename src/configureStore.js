
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';

import { persistStore, autoRehydrate } from 'redux-persist';
import reducer from './services';
import ReduxThunk from 'redux-thunk';

export default function configureStore(onCompletion: () => void):any {


  const store = createStore(reducer, undefined, compose(applyMiddleware(ReduxThunk), autoRehydrate()));

  return store;
}
