import React, { Component } from 'react';
import { View, Text, Button,Image } from 'react-native';

import TabImage from './components/tabimage';
import DefaultText from './components/defaulttext';
import Header from '../../components/header';

import { ALERTS_ICON } from '../../utils/images';
import { ALERTS_LABEL } from '../labels';

export default class Alerts extends Component {

  static navigationOptions = ({ navigation }) => {

    return {

      tabBarIcon: ({ focused }) => {

          return (
            <TabImage
              icon = { ALERTS_ICON }
              focused = { focused }
            />
          )
      }
    };
  };

  render() {

    console.log("propsreceived", this.props);

    return (

      <View>

        <Header
          isBackButtonRequired
          heading = { ALERTS_LABEL }
        />

        <DefaultText
          text = { ALERTS_LABEL }
        />

      </View>

    );

  }

}
