import React, { Component } from 'react';
import { View, Text, Button,Image } from 'react-native';

import TabImage from './components/tabimage';
import DefaultText from './components/defaulttext';
import Header from '../../components/header';

import { DASHBOARD_ICON } from '../../utils/images';
import { DASHBOARD_LABEL } from '../labels';

export default class Dashboard extends Component {

  static navigationOptions = ({ navigation }) => {

    return {

      tabBarIcon: ({ focused }) => {

          return (
            <TabImage
              icon = { DASHBOARD_ICON }
              focused = { focused }
            />
          )
      }
    };
  };

  render() {

    console.log("propsreceived", this.props);

    return (

      <View>

        <Header
          isBackButtonRequired
          heading = { DASHBOARD_LABEL }
        />

        <DefaultText
          text = { DASHBOARD_LABEL }
        />

      </View>

    );

  }

}
