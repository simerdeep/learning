import React, { Component } from 'react';
import { View, Text, Button,Image } from 'react-native';

import TabImage from './components/tabimage';
import DefaultText from './components/defaulttext';
import Header from '../../components/header';

import { HOME_ICON } from '../../utils/images';

import { HOME_LABEL } from '../labels';

export default class Home extends Component {

  static navigationOptions = ({ navigation }) => {

    return {

      tabBarIcon: ({ focused }) => {

          return (
            <TabImage
              icon = { HOME_ICON }
              focused = { focused }
            />
          )
      },
      header: false,
    };
  };

  render() {

    console.log("propsreceived", this.props);

    return (

      <View>

        <Header
          isBackButtonRequired
          heading = { HOME_LABEL }
        />

        <DefaultText
          text = { HOME_LABEL }
        />

      </View>

    );

  }

}
