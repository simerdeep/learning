import React, { Component } from 'react';
import { View, Text, Button,Image } from 'react-native';

import TabImage from './components/tabimage';
import DefaultText from './components/defaulttext';
import Header from '../../components/header';

import { SUBSCRIBE_ICON } from '../../utils/images';
import { SUBSCRIBE_LABEL } from '../labels';

export default class Subscribe extends Component {

  static navigationOptions = ({ navigation }) => {

    return {

      tabBarIcon: ({ focused }) => {

          return (
            <TabImage
              icon = { SUBSCRIBE_ICON }
              focused = { focused }
            />
          )
      }
    };
  };

  render() {

    console.log("propsreceived", this.props);

    return (
      <View>

        <Header
          isBackButtonRequired
          heading = { SUBSCRIBE_LABEL }
        />

        <DefaultText
          text = { SUBSCRIBE_LABEL }
        />

      </View>
    );

  }

}
