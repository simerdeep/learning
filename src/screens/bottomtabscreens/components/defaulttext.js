import React, { Component } from 'react';
import { Text } from 'react-native';

import * as appstyleguide from '../../../appstyleguide';

export default class DefaultText extends Component {

  render() {

    console.log("propsreceived", this.props);

    let { text } = this.props;

    return (
      <Text style = {{ alignSelf : "center", marginTop: 10 }}>
      { text }
      </Text>

    );

  }

}
