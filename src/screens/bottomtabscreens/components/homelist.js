import React, { Component } from 'react';
import { ScrollView } from 'react-native';

import { withNavigation } from 'react-navigation';

import { ALLOW_CALLS_ICON, BLOCK_CALLS_ICON, PHONEBOOK_ICON, MANAGE_PROFILE_ICON, SCHEDULE_PROFILE_ICON } from '../../../utils/images';

import HomeListItem from './homelistitem';

import styles from './styles/homeliststyles';
import * as labels from '../../labels';

class HomeList extends Component {

  render() {

    console.log("propsreceived", this.props);

    return (

      <ScrollView contentContainerStyle = { styles.mainViewStyle }>

        <HomeListItem
          icon = { MANAGE_PROFILE_ICON }
          label = { labels.MANAGE_PROFILE_LABEL }
          onPressForwardIcon = { () => this.props.navigation.navigate("manageProfile") }

        />

        <HomeListItem
          icon = { PHONEBOOK_ICON }
          label = { labels.PHONEBOOK_CALLS_LABEL }
          customViewStyle = { styles.homelistitemStyle }
        />

        <HomeListItem
          icon = { BLOCK_CALLS_ICON }
          label = { labels.BLOCK_CALLERS_LABEL }
          customViewStyle = { styles.homelistitemStyle }

        />

        <HomeListItem
          icon = { ALLOW_CALLS_ICON }
          label = { labels.ALLOW_CALLS_LABEL }
          customViewStyle = { styles.homelistitemStyle }

        />

        <HomeListItem
          icon = { SCHEDULE_PROFILE_ICON }
          label = { labels.SCHEDULE_PROFILE_LABEL }
          customViewStyle = { styles.homelistitemStyle }

        />

      </ScrollView>

    );

  }

}

export default withNavigation(HomeList);
