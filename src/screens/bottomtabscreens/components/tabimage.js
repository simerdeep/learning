import React, { Component } from 'react';
import { Image } from 'react-native';

import * as appstyleguide from '../../../appstyleguide';

export default class TabImage extends Component {

  render() {

    console.log("propsreceived", this.props);

    let { icon, focused } = this.props;

    return (
      <Image
        source = { icon }
        style = {{ tintColor: focused ? appstyleguide.APP_RED_COLOR : appstyleguide.APP_BLACK_COLOR  }}
      />

    );

  }

}
