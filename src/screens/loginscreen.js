import React, { Component } from 'react';
import { View, Text, Button,Image, TextInput, Alert } from 'react-native';

import { getDeviceDimensions } from '../utils';

export default class LoginScreen extends Component {

  constructor(props, context){

    super(props, context);

    this.state = {

      mobileNumber : "",
      password : ""

    };
  }

  render() {

    console.log("propsreceived", this.props);

    return (

      <View style = {{ margin: 10, justifyContent: "center", height: getDeviceDimensions().deviceHeight, width: getDeviceDimensions().deviceWidth -20 }}>

        <TextInput
          style = {{ height: 40, borderColor: 'gray', borderWidth: 1 }}
          onChangeText = {(mobileNumber) => this.setState({mobileNumber})}
          value = { this.state.mobileNumber }
          placeholder = "Enter mobile number"
          keyboardType = "phone-pad"
        />

        <TextInput
          style = {{ height: 40, borderColor: 'gray', borderWidth: 1, marginTop : 10 }}
          onChangeText = {(password) => this.setState({password})}
          value = { this.state.password }
          placeholder = "Enter Password"
        />

        <View style = {{ marginTop: 10 }}>

          <Button
            onPress = { () => this.loginUser() }
            title="Login"
            color="#841584"
          />

        </View>

      </View>

    );

  }

  loginUser() {

    if(this.state.mobileNumber == "9717683803" && this.state.password == "1234")
      this.props.navigation.navigate("bottomNavigation");
    else {

      Alert.alert(
        'Login',
        'Incorrect username and password',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
      );
    }

  }

}
