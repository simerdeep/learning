import * as appActionTypes from './appactiontypes';

export function updateIntroSliderFlag(): Action {

  return ({
          type: appActionTypes.UPDATE_INTRO_SHOW_FLAG,
          response: false,
  });

}
