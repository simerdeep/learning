/*
Imported Libraries
*/
import * as appActionTypes from './appactiontypes';
import React from 'react';

const initialState = {

  errorCounter: 0,

  isIntroToShow : true
};

// Reducer for authentication
export default function appReducer(state = initialState, action){

  console.log(action.type);
  console.log(state);

  switch (action.type) {

    case appActionTypes.UPDATE_INTRO_SHOW_FLAG:


      return {
        ...state,
        isIntroToShow: action.response,
      };

      break;

      // For rehydrate
    case "persist/REHYDRATE":

      return {
        ...state,
        ...action.payload.app,
      };
      break;

    default:

      return {
        ...state,
      };

  }

}
