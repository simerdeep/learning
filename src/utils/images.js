
/*Bottom tab icons*/
export const HOME_ICON = require('../img/bottomtabicons/homeicon.png') ;
export const ALERTS_ICON = require('../img/bottomtabicons/alerts.png') ;
export const SUBSCRIBE_ICON = require('../img/bottomtabicons/subscribe.png') ;
export const DASHBOARD_ICON = require('../img/bottomtabicons/dashboard.png') ;

/*Common icons*/
export const BACK_ARROW_ICON = require('../img/commonicons/backarrow.png') ;
export const FORWARD_ARROW_ICON = require('../img/commonicons/forwardarrow.png') ;
export const PROFILE_PIC_ICON = require('../img/commonicons/profilepic.png') ;
export const PROFILE_DESIGN_ICON = require('../img/commonicons/profiledesign.jpg') ;
export const EDIT_ICON = require('../img/commonicons/editicon.png') ;

/*ScrollView icons*/
export const SCROLL_ICON_1 = require('../img/commonicons/scrollicon1.png') ;
export const SCROLL_ICON_2 = require('../img/commonicons/scrollicon2.png') ;
export const SCROLL_ICON_3 = require('../img/commonicons/scrollicon3.png') ;
export const SCROLL_ICON_4 = require('../img/commonicons/scrollicon4.png') ;
export const SCROLL_ICON_5 = require('../img/commonicons/scrollicon5.png') ;
export const SCROLL_ICON_6 = require('../img/commonicons/scrollicon6.png') ;
export const SCROLL_ICON_7 = require('../img/commonicons/scrollicon7.png') ;
export const SCROLL_ICON_8 = require('../img/commonicons/scrollicon8.png') ;
export const SCROLL_ICON_9 = require('../img/commonicons/scrollicon9.png') ;
export const SCROLL_ICON_10 = require('../img/commonicons/scrollicon10.png') ;
