import { Dimensions } from 'react-native';

const PRODUCTION_MODE = true;

if(PRODUCTION_MODE)
  console.log = () => {};

export function getDeviceDimensions() {

  let deviceHeight = parseInt(Dimensions.get('window').height);
  let deviceWidth = parseInt(Dimensions.get('window').width);

  let deviceDimensionObject = {

    deviceHeight: deviceHeight,
    deviceWidth: deviceWidth

  };

  return deviceDimensionObject;

}
